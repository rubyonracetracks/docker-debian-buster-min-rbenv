# Docker Debian Buster - Minimal - rbenv

This repository is used for building the Minimal Debian Buster Docker image with rbenv for [Ruby on Racetracks](https://www.rubyonracetracks.com/).

## Name of This Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rbenv/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-stage2](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-stage2/container_registry)

## What's Added
rbenv

## Things NOT Included
Specific versions of Ruby

## What's the Point?
* This Docker image is a building block for other Docker images for [Ruby on Racetracks](https://www.rubyonracetracks.com/).
* Specific versions of Ruby are NOT included in this Docker image.  (This is covered in downstream Docker images.)

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
